﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paizano.Common.Constants
{
    public class ApiUrls
    {
        public const string BaseUrlWeb = "http://paizano.checkyourproject.com";
        public const string BaseUrl = "http://paizano.checkyourproject.com/DesktopModules/PaizanoAPI/API/Service/";
        public const string Get_countrylist = BaseUrl + "GetAllCountry";
        public const string Get_statelist = BaseUrl + "GetAllStateByCountryID";
        public const string Get_citylist = BaseUrl + "GetAllCityByCountryStateID";
        public const string Get_securityquestuionlist = BaseUrl + "GetSecurityquestions";
        public const string CreateUser = BaseUrl + "CreateUserWithSecurity";
        public const string LoginUrl = "http://paizano.checkyourproject.com/DesktopModules/JwtAuth/API/mobile/login";
        public const string Url_Login_Extend = BaseUrlWeb + "/DesktopModules/JwtAuth/API/mobile/extendtoken";
        public const string GetTransCategory = BaseUrl + "GetTransactionCategory";
        public const string Url_CreateTransaction = BaseUrl + "CreateTransaction";
        public const string Url_TransactionHistory = BaseUrl + "GetTransactionDetail";
        public const string Url_ReceivedHistory = BaseUrl + "GetReceivedTransactions";
        public const string Url_AcceptRejectTransaction = BaseUrl + "AcceptRejectTransaction";
        public const string Url_Forgetpassword = BaseUrl + "PasswordRestLink";
        public const string Url_Checkuserexist = BaseUrl + "CheckUserExist";
        public const string Url_UpdatePassandsecurity = BaseUrl + "UpdatePasswordAndSecurity";
        public const string Url_GetProfile = BaseUrl + "GetProfile";
        public const string Url_UpdateProfile = BaseUrl + "UpdateProfile";
        public const string Url_GetSecurityquestions = BaseUrl + "GetSecurityquestions";
        public const string Url_UpdatePasswordAndSecurity = BaseUrl + "UpdatePasswordAndSecurity";
        public const string Url_GetAllFavorite = BaseUrl + "GetAllFevoriteNumbers";
        public const string Url_GetLedgerDetail = BaseUrl + "GetTrasactionDetails_BYID";
        public const string Url_GetOneSignalID = "https://onesignal.com/api/v1/players";
        public const string URl_OTP = "http://121.241.242.102:8080/OtpApi/otpgenerate?username=vmcotp&password=vmcotp12&msisdn={0}&msg={1}&source=VMCOTP&otplen=6&tagname=vmcotp&exptime=60";
        public const string URl_VerifyOTP = "http://121.241.242.102:8080/OtpApi/checkotp?username=vmcotp&password=vmcotp12&msisdn={0}&otp={1}";
    }
}
