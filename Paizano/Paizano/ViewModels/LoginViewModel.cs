﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;
using Paizano.Models;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Paizano.Common.Constants;
using Paizano.Helpers;

namespace Paizano.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private INavigation _navigation;
        public LoginViewModel(INavigation navigation)
        {
            this._navigation = navigation;
            TapForget = new Command(ExecuteonForget);
        }


        private UserDetail _UserDetail=new UserDetail();
        public UserDetail userDetail
        {
            get { return _UserDetail; }
            set { _UserDetail = value; OnPropertyChanged("userDetail"); }
        }

        private bool _rememberMe;
        public bool RememberMe
        {
            get { return _rememberMe; }
            set
            {
                _rememberMe = value;
                OnPropertyChanged();
            }
        }

        public Command TapForget { get; set; }



        public async void ExecuteOnsignup()
        {
            await _navigation.PushAsync(new Views.SignUpPage());
        }

        public async void ExecuteOnLogin()
        {
            try
            {
                string sss = Settings.PZN_DeviceID;
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.LoginUrl, string.Empty);
                var userjson = JsonConvert.SerializeObject(userDetail);
                var response = await apicall.Post<UserLoginResponse>(userjson);
                if (response!= null )
                {
                    if(response?.userId!=null && !string.IsNullOrEmpty(response.userId))
                    {
                        UserDialogs.Instance.Loading().Hide();
                        Settings.PZN_UserID = response.userId.ToString();
                        Settings.PZN_DisplayName = response.displayName;
                        Settings.PZN_Piclink = "http://paizano.checkyourproject.com/profilepic.ashx?userId="+Settings.PZN_UserID+"&h=80&w=80";
                        // Settings.PZN_Currency = response.data.Currency;
                        Settings.AccessTokenSettings = response.accessToken.ToString();
                        Settings.RenewalTokenSettings = response.renewalToken.ToString();
                        Settings.PZN_Mobile=userDetail.u;
                        if (RememberMe)
                        {
                            
                            Settings.PZN_Password = userDetail.p;
                        }
                        else
                        {
                            
                            Settings.PZN_Password = String.Empty;
                        }


                        Onsuccessfull();
                    }
                    else
                    {
                        UserDialogs.Instance.Loading().Hide();
                        UserDialogs.Instance.Alert("Please Enter Valid ID and Password", "Message", "Ok");
                    }
                }
                else
                {
                    UserDialogs.Instance.Loading().Hide();
                    UserDialogs.Instance.Alert("Please Enter Valid ID and Password", "Message", "Ok");
                }
            }
            catch(Exception e)
            {
                UserDialogs.Instance.Loading().Hide();
                UserDialogs.Instance.Alert(e.Message, "Error", "Ok");
            }
        }

        private async void Onsuccessfull()
        {
            await _navigation.PushAsync(new Views.MenuNavigationPage());
        }


        private async void ExecuteonForget()
        {
            await App.Current.MainPage.Navigation.PushAsync(new Views.ForgetPage());
        }
    }
}
