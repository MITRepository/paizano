﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Paizano.Common.Constants;
using Paizano.Helpers;
using Paizano.Models;
using Xamarin.Forms;

namespace Paizano.ViewModels
{
   public class ChangePassword:BaseViewModel
    {
        public ChangePassword()
        {
            
            SavePassword = new Command(ExecuteOnSave);
        }


        private string newpassword;
        public string NewPassword
        {
            get { return newpassword; }
            set { newpassword = value; OnPropertyChanged("NewPassword"); }
        }

        private string reenter;
        public string ReenterPassword
        {
            get { return reenter; }
            set { reenter = value; OnPropertyChanged("ReenterPassword"); }
        }

        public Command SavePassword { get; set; }

        
        

        public async void ExecuteOnSave()
        {
            if(NewPassword==ReenterPassword)
            {
                try
                {
                    UserAndSecurity objinfo = new UserAndSecurity();
                    objinfo.UserID =Convert.ToInt32(Settings.PZN_UserID);
                    objinfo.flag = 1;
                    objinfo.Password = NewPassword;
                    string json = JsonConvert.SerializeObject(objinfo);
                    HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Url_UpdatePassandsecurity, Settings.AccessTokenSettings);

                    var response = await apicall.Post<UserAndSecurityResponse>(json);
                    if(response!=null)
                    {
                        if(response?.status!=null && response.status=="true")

                        {
                            await App.Current.MainPage.DisplayAlert("Message","Password updated successfully.","OK");
                            await App.Current.MainPage.Navigation.PushAsync(new Views.MenuNavigationPage());
                        }else
                        {
                            await App.Current.MainPage.DisplayAlert("Message", "Password not updated.", "OK");
                        }
                    }


                }
                catch (Exception ee)
                {

                    await App.Current.MainPage.DisplayAlert("Message", ee.Message, "OK");
                }
            }else
            {
                await App.Current.MainPage.DisplayAlert("Message","Password Reenter does not match." ,"OK");
            }
        }
    }
}
