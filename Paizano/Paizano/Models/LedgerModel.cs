﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Contacts.Abstractions;

namespace Paizano.Models
{
   public class LedgerModel
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public List<Phone> Phones { get; set; }
    }
    public class FavoriteRequest
    {
        public string MobileNo { get; set; }
        public string CurrentMobile { get; set; }
    }
    public class LedgerResponse
    {
        public string status { get; set; }
        public string Message { get; set; }
        public List<FavoriteRequest> data { get; set; }
    }
}
