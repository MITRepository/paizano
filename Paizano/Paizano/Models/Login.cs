﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paizano.Models
{
   public class Login
    {
    }

    public class UserDetail
    { 
        
        public string u { get; set; }
        public string p { get; set; }
        

    }
    public class User
    {
        public int UserID { get; set; }
        public string Displayname { get; set; }
        public string Currency { get; set; }
        public string Piclink { get; set; }

    }
    public class UserLoginResponse
    {
        public string userId { get; set; }
        public string displayName { get; set; }
        public string accessToken { get; set; }
        public string renewalToken { get; set; }
    }

}
