﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paizano.Models
{
  public  class Registration
    {
    }

    public class SecurityQuestion
    {
        public int ID { get; set; }
        public string Question { get; set; }
        public int CountryID { get; set; }
    }

    public class SecurityQuestionDetailResponse
    {
        public string status { get; set; }
        public string Message { get; set; }
        public List<SecurityQuestion> data { get; set; }
    }

    public class UserAndSecurity
    {
        public int UserID { get; set; }
        public string Mobile { get; set; }
        public string Password { get; set; }
        public int QuestionID { get; set; }
        public string Answer { get; set; }
        public int flag { get; set; }
        public string device { get; set; }
        public string email { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
    }

    public class UserAndSecurityResponse
    {
        public string status { get; set; }
        public string Message { get; set; }
        public string data { get; set; }

    }

}
