﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paizano.Models
{
   public class AppModel
    {
        public string app_id { get; set; }
        public string identifier { get; set; }
        public string language { get; set; }
        public string timezone { get; set; }
        public string game_version { get; set; }
        public string device_os { get; set; }
        public int device_type { get; set; }
        public string device_model { get; set; }
        public Tags tags { get; set; }
    }

    public class Tags
    {
        public string mobile { get; set; }
    }

    public class AppResponse
    {
        public string success { get; set; }
        public string id { get; set; }
    }
}
