﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paizano.Models
{
   public class Menu
    {
        public string icon { get; set; }
        public string Title { get; set; }
        public Type TargetType { get; set; }
    }
}
