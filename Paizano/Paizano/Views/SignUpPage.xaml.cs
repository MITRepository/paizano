﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Paizano.ViewModels;
using Plugin.DeviceInfo;
using Paizano.Models;
using Paizano.Helpers;
using Acr.UserDialogs;

namespace Paizano.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignUpPage : ContentPage
    {
        private SignUpViewModel signUpViewModel;
        public SignUpPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, true);
            this.Title = "Registration";
            signUpViewModel = new SignUpViewModel();
            BindingContext = signUpViewModel;
        }



        private void pickercountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            Country country = (Country)pickercountry.SelectedItem;
            lblcurrency.Text = country.Currency;
            txtcode.Text = country.CountryCode;
            lbllanguage.Text = country.DefaultLanguage;
            StateRequest objsr = new StateRequest();
            objsr.CountryID = country.CountryID;
            objsr.device = Settings.PZN_DeviceID;
            signUpViewModel.Executestatebycountryid(objsr);


        }

        private void pickerstate_SelectedIndexChanged(object sender, EventArgs e)
        {
            Country country = (Country)pickercountry.SelectedItem;
            State state = (State)pickerstate.SelectedItem;
            CityRequest objcr = new CityRequest();
            objcr.CountryID = country.CountryID;
            objcr.StateID = state.ID;
            objcr.device = Settings.PZN_DeviceID;
            signUpViewModel.ExecuteCitybystateid(objcr);
        }

        private void btnsave_Clicked(object sender, EventArgs e)
        {
            if (pickercountry.SelectedItem == null)
            {
                UserDialogs.Instance.Alert("Please Select country", "Message", "Ok");
                return;
            }

            if (pickerstate.SelectedItem == null)
            {
                UserDialogs.Instance.Alert("Please select state.", "Message", "Ok");
                return;
            }
            if (pickercity.SelectedItem == null)
            {
                UserDialogs.Instance.Alert("Please select city.", "Message", "Ok");
                return;
            }
            if (string.IsNullOrEmpty(txtmobile.Text))
            {
                UserDialogs.Instance.Alert("Please enter mobile number.", "Message", "Ok");
                return;
            }
            Settings.PZN_Country = ((Country)pickercountry.SelectedItem).CountryName;
            Settings.PZN_State = ((State)pickerstate.SelectedItem).StateName;
            Settings.PZN_City = ((City)pickercity.SelectedItem).CityName;
            Settings.PZN_Mobile = txtmobile.Text;
            signUpViewModel.ExecuteOnSendOTP();
        }
    }
}
