﻿using Paizano.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Paizano.Models;
namespace Paizano.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReceivedHistoryPage : ContentPage
    {
        private ReceivedHistoryViewModel _transHistoryVM;
        public ReceivedHistoryPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, true);
            this.Title = "Received History";
            _transHistoryVM = new ReceivedHistoryViewModel();
            BindingContext = _transHistoryVM;
        }
        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            // don't do anything if we just de-selected the row
            TransactionModel trns = (TransactionModel)e.Item;
           
            if (trns.Status == "Pending")
            {
                _transHistoryVM.OnItemTapped(trns);
            }
            if (e.Item == null)
            {
                return;
            }
            // do something with e.SelectedItem
            ((ListView)sender).SelectedItem = null;
            // de-select the row after ripple effect
        }
    }
}
