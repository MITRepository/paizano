﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Paizano.ViewModels;
namespace Paizano.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChangePasswordPage : ContentPage
    {
        private ChangePassword _ChangePassword;
        public ChangePasswordPage()
        {
            InitializeComponent();
            this.Title = "Change Password";
            _ChangePassword = new ChangePassword();
            BindingContext = _ChangePassword;
        }
    }
}
