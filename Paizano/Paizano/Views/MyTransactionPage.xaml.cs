﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Paizano.ViewModels;
namespace Paizano.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyTransactionPage : ContentPage
    {
        private MyTransacrionPageViewModel _mytransactionViewModel;
        public MyTransactionPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, true);
            this.Title = "My Transactions";
            _mytransactionViewModel = new MyTransacrionPageViewModel();
            BindingContext = _mytransactionViewModel;
        }
    }
}
