﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Paizano.ViewModels;
namespace Paizano.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfileUpdatePage : ContentPage
    {
        private ProfileUpdateViewModel _pfUpdateViewmodel;
        public ProfileUpdatePage()
        {
            InitializeComponent();
            NavigationPage.SetHasBackButton(this, false);
            this.Title = "Profile Update";
            _pfUpdateViewmodel = new ProfileUpdateViewModel();
            BindingContext = _pfUpdateViewmodel;
        }
    }
}
