﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Paizano.ViewModels;
using Paizano.Models;
namespace Paizano.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChangeSecurityPage : ContentPage
    {
        private ChangeSecurityViewModel _csviewmodel;
        public ChangeSecurityPage()
        {

            InitializeComponent();
            this.Title = "Change Security Question";
            _csviewmodel = new ChangeSecurityViewModel();
            BindingContext = _csviewmodel;
        }

        private void drpques_SelectedIndexChanged(object sender, EventArgs e)
        {
            SecurityQuestion  objinfo= ((SecurityQuestion)drpques.SelectedItem);
            _csviewmodel.QuestionID = objinfo.ID.ToString();
        }
    }
}
